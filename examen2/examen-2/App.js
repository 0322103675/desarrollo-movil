import React, { useState, useEffect } from 'react';
import { View, TextInput, Button, StyleSheet, Text, Alert, Image } from 'react-native';
import { initializeApp } from 'firebase/app';
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword, onAuthStateChanged, signOut } from 'firebase/auth';

// Configuración de Firebase
const firebaseConfig = {
  apiKey: "AIzaSyDwnQ_UYXjavoO-BshMxxGhQGS0LnTAZzQ",
  authDomain: "examen2-10135.firebaseapp.com",
  projectId: "examen2-10135",
  storageBucket: "examen2-10135.appspot.com",
  messagingSenderId: "969639237770",
  appId: "1:969639237770:web:fc62dc7677b1f6fecd7573",
  measurementId: "G-9HV7MJR8NY"
};


const app = initializeApp(firebaseConfig);
const auth = getAuth(app);

const App = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isLogin, setIsLogin] = useState(true);
  const [user, setUser] = useState(null);

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      setUser(user);
    });

    return unsubscribe;
  }, []);

  const handleAuthentication = () => {
    if (isLogin) {
      signInWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
          setUser(userCredential.user);
        })
        .catch((error) => {
          Alert.alert('Error', error.message);
        });
    } else {
      createUserWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
          setUser(userCredential.user);
        })
        .catch((error) => {
          Alert.alert('Error', error.message);
        });
    }
  };

  const handleLogout = () => {
    signOut(auth)
      .then(() => {
        setUser(null);
      })
      .catch((error) => {
        Alert.alert('Error', error.message);
      });
  };

  return (
    <View style={styles.container}>
      {user ? (
        <AuthenticatedScreen user={user} handleLogout={handleLogout} />
      ) : (
        <AuthScreen
          email={email}
          setEmail={setEmail}
          password={password}
          setPassword={setPassword}
          isLogin={isLogin}
          setIsLogin={setIsLogin}
          handleAuthentication={handleAuthentication}
        />
      )}
    </View>
  );
};

const AuthScreen = ({ email, setEmail, password, setPassword, isLogin, setIsLogin, handleAuthentication }) => (
  <View style={styles.authContainer}>
    <Image source={require('./assets/parra.webp')} style={styles.logo} />
    <Text style={styles.title}>{isLogin ? 'Sign In to Parra’s Dev' : 'Sign Up to Parra’s Dev'}</Text>
    <TextInput
      style={styles.input}
      value={email}
      onChangeText={setEmail}
      placeholder="Email"
      autoCapitalize="none"
      placeholderTextColor="#aaa"
    />
    <TextInput
      style={styles.input}
      value={password}
      onChangeText={setPassword}
      placeholder="Password"
      secureTextEntry
      placeholderTextColor="#aaa"
    />
    <View style={styles.buttonContainer}>
      <Button title={isLogin ? 'Sign In' : 'Sign Up'} onPress={handleAuthentication} color="#3498db" />
    </View>
    <Text style={styles.toggleText} onPress={() => setIsLogin(!isLogin)}>
      {isLogin ? 'Need an account? Sign Up' : 'Already have an account? Sign In'}
    </Text>
  </View>
);

const AuthenticatedScreen = ({ user, handleLogout }) => (
  <View style={styles.authContainer}>
    <Image source={require('./assets/parra.webp')} style={styles.welcomeImage} />
    <Text style={styles.title}>Welcome to Parra’s Dev, {user.email}!</Text>
    <Button title="Logout" onPress={handleLogout} color="#e74c3c" />
  </View>
);

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 16,
    backgroundColor: '#283747',
  },
  authContainer: {
    width: '80%',
    maxWidth: 400,
    backgroundColor: '#ffffff',
    padding: 20,
    borderRadius: 10,
    elevation: 5,
  },
  title: {
    fontSize: 24,
    marginBottom: 20,
    textAlign: 'center',
    color: '#2c3e50',
  },
  input: {
    height: 40,
    borderColor: '#3498db',
    borderWidth: 1,
    marginBottom: 20,
    padding: 8,
    borderRadius: 4,
    color: '#2c3e50',
  },
  buttonContainer: {
    marginBottom: 20,
  },
  toggleText: {
    color: '#3498db',
    textAlign: 'center',
    marginTop: 10,
  },
  logo: {
    width: 100,
    height: 100,
    marginBottom: 20,
    alignSelf: 'center',
    border: 40,
    borderRadius: 50,
  },
  welcomeImage: {
    width: 150,
    height: 150,
    marginBottom: 20,
    alignSelf: 'center',
    borderRadius: 50,
  },
});

export default App;
